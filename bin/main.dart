import 'package:leto/leto.dart';
import 'package:leto_schema/leto_schema.dart';

// dart run build_runner build --delete-conflicting-outputs
part 'main.g.dart';

@Query()
String hello(Ctx<Object?> ctx) {
  return 'Hello World';
}

@Query()
List<Person> people(Ctx ctx) {
  return [
    Person(name: 'Jim', createdAt: DateTime.now()),
    Person(name: 'Bob', createdAt: DateTime.now()),
    Person(name: 'Kevin', createdAt: DateTime.now()),
    Person(name: 'Gru', createdAt: DateTime.now()),
  ];
}

@Query()
Person? personByName(String name, Ctx ctx) {
  if (name == 'Kevin') {
    return Person(name: 'Kevin', createdAt: DateTime.now());
  }
  return null;
}

@GraphQLObject()
class Person {
  Person({
    required this.name,
    required this.createdAt,
  });

  final String name;
  final DateTime createdAt;

  @GraphQLField(name: 'pet')
  Pet? resolvePet(Ctx<Person> ctx) {
    print(ctx.args);

    if (ctx.object.name == 'Kevin') {
      return Pet('Stella');
    }
    return null;
  }
}

@GraphQLObject()
class Pet {

  final String name;

  const Pet(this.name);
}

final schema = GraphQLSchema(
  queryType: objectType('Query', fields: [
    helloGraphQLField,
    peopleGraphQLField,
    personByNameGraphQLField,
  ]),
);
final letoGraphQL = GraphQL(
  schema,
  extensions: [],
  introspect: true,
  validate: true,
);

Future<void> main(List args) async {
  GraphQLResult result = await letoGraphQL.parseAndExecute(
    'query Test(\$name: String!) { personByName(name: \$name) { name pet { name } } }',
    operationName: 'Test',
    variableValues: {
      'name': 'Kevin',
    }
  );

  print(result);
}
