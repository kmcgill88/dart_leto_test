// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'main.dart';

// **************************************************************************
// _GraphQLGenerator
// **************************************************************************

GraphQLObjectField<String, Object?, Object?> get helloGraphQLField =>
    _helloGraphQLField.value;
final _helloGraphQLField =
    HotReloadableDefinition<GraphQLObjectField<String, Object?, Object?>>(
        (setValue) => setValue(graphQLString.nonNull().field<Object?>(
              'hello',
              resolve: (obj, ctx) {
                final args = ctx.args;

                return hello(ctx);
              },
            )));

GraphQLObjectField<List<Person>, Object?, Object?> get peopleGraphQLField =>
    _peopleGraphQLField.value;
final _peopleGraphQLField = HotReloadableDefinition<
        GraphQLObjectField<List<Person>, Object?, Object?>>(
    (setValue) =>
        setValue(personGraphQLType.nonNull().list().nonNull().field<Object?>(
          'people',
          resolve: (obj, ctx) {
            final args = ctx.args;

            return people(ctx);
          },
        )));

GraphQLObjectField<Person?, Object?, Object?> get personByNameGraphQLField =>
    _personByNameGraphQLField.value;
final _personByNameGraphQLField =
    HotReloadableDefinition<GraphQLObjectField<Person?, Object?, Object?>>(
        (setValue) => setValue(personGraphQLType.field<Object?>(
              'personByName',
              resolve: (obj, ctx) {
                final args = ctx.args;

                return personByName((args["name"] as String), ctx);
              },
            ))
              ..inputs.addAll([graphQLString.nonNull().inputField('name')]));

// **************************************************************************
// _GraphQLGenerator
// **************************************************************************

final _personGraphQLType =
    HotReloadableDefinition<GraphQLObjectType<Person>>((setValue) {
  final __name = 'Person';

  final __personGraphQLType = objectType<Person>(
    __name,
    isInterface: false,
    interfaces: [],
  );

  setValue(__personGraphQLType);
  __personGraphQLType.fields.addAll(
    [
      petGraphQLType.field(
        'pet',
        resolve: (
          obj,
          ctx,
        ) {
          final args = ctx.args;

          return obj.resolvePet(ctx);
        },
      ),
      graphQLString.nonNull().field(
            'name',
            resolve: (
              obj,
              ctx,
            ) =>
                obj.name,
          ),
      graphQLDate.nonNull().field(
            'createdAt',
            resolve: (
              obj,
              ctx,
            ) =>
                obj.createdAt,
          ),
    ],
  );

  return __personGraphQLType;
});

/// Auto-generated from [Person].
GraphQLObjectType<Person> get personGraphQLType => _personGraphQLType.value;

final _petGraphQLType =
    HotReloadableDefinition<GraphQLObjectType<Pet>>((setValue) {
  final __name = 'Pet';

  final __petGraphQLType = objectType<Pet>(
    __name,
    isInterface: false,
    interfaces: [],
  );

  setValue(__petGraphQLType);
  __petGraphQLType.fields.addAll(
    [
      graphQLString.nonNull().field(
            'name',
            resolve: (
              obj,
              ctx,
            ) =>
                obj.name,
          )
    ],
  );

  return __petGraphQLType;
});

/// Auto-generated from [Pet].
GraphQLObjectType<Pet> get petGraphQLType => _petGraphQLType.value;
