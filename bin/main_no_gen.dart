import 'package:leto/leto.dart';
import 'package:leto_schema/leto_schema.dart';

import 'person.dart';

// dart run build_runner build --delete-conflicting-outputs

// String hello(Ctx<Object?> ctx) {
//   return 'Hello World';
// }
//
// List<Person> people(Ctx ctx) {

// }

Future<void> main(List args) async {
  final schema = GraphQLSchema(
    queryType: objectType('Query', fields: [
      graphQLString
          .nonNull()
          .field('hello', resolve: (obj, ctx) => 'Hello World'),
      mcPersonGraphQLType
          .nonNull()
          .list()
          .nonNull()
          .field('people', resolve: resolvePeople)
    ]),
  );

  final letoGraphQL = GraphQL(
    schema,
    extensions: [],
    introspect: true,
    validate: true,
  );

  GraphQLResult result = await letoGraphQL.parseAndExecute(
    'query Test { hello people { name pet { name } }}',
    // 'query Test { hello }',
  );

  print(result);
}
