import 'dart:async';

import 'package:leto_schema/leto_schema.dart';

import 'pet.dart';

class Person {
  final String name;
  final DateTime createdAt;
  Pet2? pet;

  Person({
    required this.name,
    required this.createdAt,
  });
}

FutureOr<List<Person>> resolvePeople(Object? parent, Ctx<Object?>? ctx) async {
  return [
    Person(name: 'Jim', createdAt: DateTime.now()),
    Person(name: 'Bob', createdAt: DateTime.now()),
    Person(name: 'Kevin', createdAt: DateTime.now()),
    Person(name: 'Gru', createdAt: DateTime.now()),
  ];
}

final GraphQLObjectType<Person> mcPersonGraphQLType = objectType<Person>(
  'Person',
  fields: [
    graphQLString.nonNull().field(
          'name',
          resolve: (Person person, Ctx ctx) => person.name,
        ),
    graphQLDate.nonNull().field(
          'createdAt',
          resolve: (Person person, Ctx ctx) => person.createdAt,
        ),
    mcPetGraphQLType.field('pet', resolve: resolvePetByPerson),
  ],
);
