
import 'dart:async';

import 'package:leto_schema/leto_schema.dart';

import 'person.dart';

class Pet2 {
  final String name;

  const Pet2(this.name);
}


FutureOr<Pet2?> resolvePetByPerson(Person person, Ctx ctx) async {
  if (person.name == 'Kevin') {
    return Pet2('Stella');
  }
  // return Pet2('Karen');
  return null;
}

final GraphQLObjectType<Pet2> mcPetGraphQLType = objectType<Pet2>(
  'Pet2',
  fields: [
    graphQLString.nonNull().field(
      'name',
      resolve: (Pet2 pet, Ctx ctx) => pet.name,
    ),
  ],
);